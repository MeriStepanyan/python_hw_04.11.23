# 1. Write a Python program to test whether a passed letter is a vowel or not.

char=input("Please enter a letter to check whether it is vowel or consonant: ")
if(char=="a" or char=="e" or char=="i" or char =="o" or char=="u"):
    print(f"The '{char}' letter is vowel")
else:
    print(f"The '{char}' letter is consonant") 

# 2.Write a Python program to count the number of occurrences of a specific character in a string.
s_char="a"
str="The Earth is an irregularly shaped ellipsoid"   
count=0 
for i in range(len(str)-1):
    if(str[i]==s_char):
        count+=1
print(f"The number of occurrences of '{s_char}' character in the string is: ",count)   

# 3. Write a Python program to check whether a string is numeric.
string="123h"
if(string.isnumeric()):
    print("Yes, the given string is numeric")
else:
    print("No, the given string isn't numeric")

# 4. Write a Python program that will convert lowercase to uppercase in an array of symbols.

array=["abcd","efgh","iJKl"] 
arr_string=" "
for i in array:
    arr_string+=i 
print("The array's uppercased symbols converted to string : ",arr_string.upper()) 
arr_string1=" " 
arr_string1=(",".join([i for i in array]))
print("The array's uppercased symbols are: ",list(arr_string1.upper().split(',')))   

# 5.Write a Python program to get a single string from two given strings, separated by a space.
part1="Be yourself"
part2="everyone else is already taken."
                             #version 1
part12=part1+","+part2
print("Single string- ", part12)
                              #version 2
part_12=",".join([part1,part2])
print("Single string- ",part_12)


# 6. Write a Python program to add 'ing' at the end of a given string (length should be at least 3). 
# If the given string already ends with 'ing', add 'ly' instead. If the string length of the given string is less than 3, leave it unchanged.
g_string="accord"
if(g_string.endswith("ing")):
    g_string+="ly"
    print("The new string with adding 'ly",g_string)
elif(len(g_string)>=3):
     g_string+="ing"
     print("The new string with adding 'ing'->",g_string)
else:
    print("The string wasn't changed->",g_string)    
    
# 7. Implement the 'rstrip()' method.
rs_string="   apple, n"
if(rs_string.endswith(", n")):
    new_string1=rs_string[:-len(", n")] # Version 1
    new_string2=rs_string.removesuffix(", n")  # Version 2 , Սա կարելի է՞՞՞՞՞՞՞՞՞՞՞՞՞՞՞՞՞՞՞
    print("The new string: ",new_string1)    
    print("The new string: ",new_string2)    

# 8 . Write a Python program that concatenates uncommon characters from two strings.
str1="Hello Adam"
str2="Hello jane"
new_str1=" "
for i in range(len(str1)):
    if str1[i] in str2:
        continue
    else:
        new_str1+=str1[i]
for j in range(len(str2)):  
    if str2[j] in str1:
        continue
    else:
        new_str1+=str2[j]      
print(new_str1)

# 9. Write a Python program to find the common values that appear in two given strings.
line1="Hello Ann"
line2="Hello Elliot"
new_line=" "
for i in range(len(line1)):
    if line1[i] in line2 and line1[i] not in new_line:
        new_line+=line1[i]
    
for j in range(len(line2)):  
    if line2[j] in line1 and line2[j] not in new_line:
        new_line+=line2[j]
print(new_line)

# 10. Write a function that takes two sets as input and returns a new set containing the elements 
# that are present in both sets.
set1={'a','b','c'}
set2={1,2,3}
set12=set1.union(set2)
print(set12)

# 11. Write a function that takes two sets of integers and performs a bitwise OR operation on the elements of both sets. Return the resulting set.
def sets_bit_or(set1,set2):
    ls1=list(set1)
    ls2=list(set2)
    ls12=[]
    for i in range(len(ls1)):
        ls12.append(ls1[i]|ls2[i])
    set12=set(ls12)
    return set12
st1={1,2,3}
st2={4,5,2}
print(sets_bit_or(st1,st2))
# 12.Create a function that checks if a given integer is even or odd using bitwise operators.

def check_bit_odd_even(n):
    if(n&1==1):
        print("The given number is odd")
    elif(n&1==0):
        print("The given number is even")  
check_bit_odd_even(170)    

# 13.Write a Python program to swap two numbers using bitwise operators.
def bitwise_swap(x,y):
     xy_xor=x^y
     x=xy_xor^x
     y=xy_xor^y
     return x,y
print(bitwise_swap(505,104))



